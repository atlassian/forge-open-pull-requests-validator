import config from "./config";
import {getAllOpenPullRequests} from "./bitbucket";

const repositories = process.env.REPOS || config.REPOS;
const testIssueKey = process.env.TEST_ISSUE_KEY || config.TEST_ISSUE_KEY;

function validationResult(transitionApproved, errorMessage = null) {
    return {
        result: transitionApproved,
        errorMessage: errorMessage
    }
}

export async function validatorTestWebTrigger(request) {

    const response = await validate(request.body || {issue: {key: testIssueKey}});

    return {
        body: JSON.stringify(response),
        headers: {
            'Content-Type': ['application/json']
        },
        statusCode: 200,
        statusText: 'OK'
    };
}

export async function validate(arg) {

    const issueKey = arg && arg.issue && arg.issue.key;
    if (!issueKey) {
        return validationResult(false, "Invalid argument. Expected to have the `issue.key` path in the object, got: " + JSON.stringify(arg));
    }

    try {
        const openedPullRequests = await getAllOpenPullRequests(repositories, issueKey);
        if (openedPullRequests.length === 0) {
            return validationResult(true);
        } else {
            return validationResult(false, transitionRejectedMessage(openedPullRequests));
        }
    } catch (err) {
        console.log("Exception caught", err);
        return validationResult(false, err.message || "Unknown error occurred, consult the Forge app logs for more details.");
    }
}

function transitionRejectedMessage(openedPullRequests) {
    const count = openedPullRequests.length;
    const verb = count === 1 ? "is" : "are";
    const plural = count === 1 ? "" : "s";
    const pullRequestTitles = openedPullRequests.map(pr => `"${pr.title}"`).join(", ");
    return `There ${verb} ${count} opened pull request${plural} for this issue: ${pullRequestTitles}.`;
}


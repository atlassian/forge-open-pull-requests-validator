import api from '@forge/api'

const username = process.env.USERNAME;
const password = process.env.PASSWORD;

export async function getAllOpenPullRequests(repositories, issueKey) {
    const query = encodeURI(`state = "OPEN" AND destination.branch.name = "master" AND source.branch.name ~ "${issueKey}"`);

    const allPullRequests = await Promise.all(repositories.split(",").map(async repository =>
        await getPullRequestsByQuery(repository.trim(), query)));

    return allPullRequests.flat();
}

async function getPullRequestsByQuery(repository, query) {
    const path = `https://api.bitbucket.org/2.0/repositories/${repository}/pullrequests?q=${query}`;
    const headers = {
        'Authorization': 'Basic ' + Buffer.from(username + ":" + password).toString('base64')
    };
    const result = await api.fetch(path, {headers});
    const json = await result.json();
    return json.values;
}

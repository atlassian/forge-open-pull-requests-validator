# Forge Open Pull Requests Workflow Validator

[![Atlassian license](https://img.shields.io/badge/license-Apache%202.0-blue.svg?style=flat-square)](LICENSE) [![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](CONTRIBUTING.md)

This simple Forge app adds a workflow validator that you can configure in your classic project workflows. 
The validator checks to ensure there are no open pull requests where the issue key is part of the pull request title.


## Usage

* Go to your workflow configuration and edit the workflow for your project.
* Select a transition you wish to add the workflow validator to.
* Find the workflow validator on the list of validators and add it to your transition.
* Publish the workflow.

Consult the [Advanced workflow configuration](https://confluence.atlassian.com/adminjiracloud/advanced-workflow-configuration-776636620.html#Advancedworkflowconfiguration-validatorAddingavalidator) documentation for more details.


## Requirements

See [Set up Forge](https://developer.atlassian.com/platform/forge/set-up-forge/) for instructions to get set up.

## Quick start

Once you have logged into the CLI (`forge login`), follow the steps below to install the app onto your site:

1. Clone this repository
2. Run `forge register` to register a new copy of this app to your developer account
3. Run `npm install` to install your dependencies
4. Run `forge deploy` to deploy the app into the default environment
5. Run `forge install` and follow the prompts to install the app

### Environment variables

The following environment variables are used by this app:

* USERNAME - your Bitbucket username.
* PASSWORD - a Bitbucket [app password](https://confluence.atlassian.com/bitbucket/app-passwords-828781300.html) to be used by this app.
* REPOS - a comma-separated list of repositories to look for pull requests in. 
          Each repository consists of a workspace followed by a slash, and then a repository name, 
          like this: "workspace-name/repository-name".
          Default value for this variable can be set in `src/config.js`.
* TEST_ISSUE_KEY - a default issue key used when testing the validator with the web trigger.
                   Default value for this variable can be set in `src/config.js`.  

Invoke these commands to set values of all the variables:

```
forge variables:set USERNAME value
forge variables:set PASSWORD value --encrypt
forge variables:set REPOS value
forge variables:set TEST_ISSUE_KEY value
```

Note, when tunneling you will need to set your environment variables locally with the `FORGE_USER_VAR_` prefix:

```
export FORGE_USER_VAR_USERNAME=value
export FORGE_USER_VAR_PASSWORD=value
export FORGE_USER_VAR_REPOS=value
export FORGE_USER_VAR_TEST_ISSUE_KEY=value
```

Consult the [documentation](https://developer.atlassian.com/platform/forge/environments/) for more information on environment variables.

## Documentation

The app consists of one workflow validator and one webtrigger defined in the `manifest.yaml` file.
* `no-open-prs-validator`: a workflow validator that checks if there are no opened pull requests for the issue being transitioned. 
  It's implemented with the `validatorTestWebTrigger` function in the `index.js` file. 
* `validator-test-webtrigger`: a webtrigger that can be used to test the validator without a need to perform a workflow transition. 
                               See the [Tests](#Tests) section for more details.
  
The projects consists of three JavaScript files:
* `index.js`: contains implementation of the two modules defined in the manifest.
* `bitbucket.js`: a set of functions for communication with Bitbucket.
* `config.js`: default values for some environment variables 
               (note that credentials to Bitbucket don't have default value defined in code and must always be set using
                the environment variables mechanism).   

## Tests

The app includes a web trigger that can be used to easily test the validator logic.
Use environment variables or edit the test issue key in `src/config.js` to set the issue key used in the test.

To invoke the web trigger:

* invoke `forge install:list` in the terminal to get the installation ID associated with the app installation on your instance.
* use that ID as input for `forge webtrigger` to get the web trigger URL.
* open the URL in the browser or send a GET request to it by other means.

## Contributions

Contributions to Forge Open Pull Requests Workflow Validator are welcome! Please see [CONTRIBUTING.md](CONTRIBUTING.md) for details. 

## License

Copyright (c) 2020 Atlassian and others.
Apache 2.0 licensed, see [LICENSE](LICENSE) file.
